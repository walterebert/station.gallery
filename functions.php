<?php
/**
 * Theme functions
 *
 * @package station.gallery
 */

if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

/**
 * Do not load theme fonts from Google
 */
function renkon_fonts_url() {
	return '';
}

/* Load custom styles and scripts */
add_action(
	'wp_enqueue_scripts',
	function () {
		// Only load lightbox on pages and posts.
		if ( is_single() ) {
			$simplelightbox_version = '1.17.2';
			wp_enqueue_style(
				'simple-lightbox',
				get_stylesheet_directory_uri() . '/vendor/simplelightbox.min.css',
				[],
				$simplelightbox_version
			);

			wp_enqueue_script(
				'simple-lightbox',
				get_stylesheet_directory_uri() . '/vendor/simple-lightbox.min.js',
				[ 'jquery-core' ],
				$simplelightbox_version,
				true
			);

			wp_add_inline_script( 'simple-lightbox', 'jQuery(".wp-block-gallery a").simpleLightbox();' );
		}

		// Load parent stylesheet.
		wp_enqueue_style(
			'parent',
			get_template_directory_uri() . '/style.css',
			[],
			wp_get_theme( 'renkon' )->get( 'Version' )
		);

		// Load theme stylesheet.
		// Child style handle must be named 'style'. Otherwise it will be loaded twice, because stylesheet has no version number in parent theme.
		wp_enqueue_style(
			'style',
			get_stylesheet_directory_uri() . '/style.css',
			[ 'parent' ],
			wp_get_theme()->get( 'Version' )
		);
	}
);

/* Remove unused taxonomies */
add_action(
	'init',
	function () {
		unregister_taxonomy_for_object_type( 'post_tag', 'post' );
	}
);

/* Increase image quality */
add_filter(
	'jpeg_quality',
	function () {
		return 85;
	}
);

/* Remove/customise theme features */
add_action(
	'after_setup_theme',
	function () {
		remove_theme_support( 'post-formats' );
		remove_theme_support( 'custom-background' );
		remove_theme_support( 'custom-header' );
	},
	11,
	0
);
