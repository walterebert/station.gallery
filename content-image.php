<?php
/**
 * Image content.
 *
 * @package station.gallery
 */

$options = get_option( 'renkon_theme_options' );
$post_id = get_the_ID();
?>

<article id="post-<?= esc_attr( $post_id ); ?>" <?php post_class( 'postblog' ); ?>>

	<div class="overlay">
		<header class="entry-header">
			<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
				<div class="featured-post"><?php echo esc_html( __( 'Featured', 'renkon' ) ); ?></div>
			<?php endif; ?>
			<h2 id="post-title-<?= esc_attr( $post_id ); ?>" class="entry-title"><?php the_title(); ?></h2>
		</header><!--end .entry-header -->

		<footer class="entry-meta">
			<div class="entry-date"><?php echo esc_html( get_the_date() ); ?></div>
			<div class="view-post"><a href="<?php echo esc_url( get_permalink() ); ?>" aria-labelledby="post-title-<?php the_ID(); ?>"><?php echo esc_html( __( 'View Post', 'renkon' ) ); ?></a></div>
		</footer><!-- end .entry-meta -->
	</div><!-- end .overlay -->

	<div class="thumb-wrap">
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'thumbnail' );
	} else {
		echo '<img src="';
		echo esc_url( catch_first_image() );
		echo '" alt="" />';
	}
	?>
	</div><!-- end .thumb-wrap -->

</article><!-- end post -<?= esc_attr( $post_id ); ?> -->
