# Theme for station.gallery

WordPress child theme for [station.gallery](https://station.gallery/) based on the Renkon theme by Elmastudio.

## Licenses

- Theme: [GPL-2.0-or-later](https://spdx.org/licenses/GPL-2.0-or-later.html)
- Lato font: [SIL Open Font License, 1.1](http://scripts.sil.org/OFL)
- Playfair Display font: [SIL Open Font License, 1.1](http://scripts.sil.org/OFL)

Fonts customised with [google-webfonts-helper](https://google-webfonts-helper.herokuapp.com/fonts).

## Author

[Walter Ebert](https://walterebert.com/)
