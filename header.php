<?php
/**
 * Page header
 *
 * @package station.gallery
 */

$options = get_option( 'renkon_theme_options' );
?>
<!DOCTYPE html>
<html  id="doc" class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<?php if ( ! empty( $options['custom_favicon'] ) ) : ?>
<link rel="shortcut icon" type="image/ico" href="<?php echo esc_attr( $options['custom_favicon'] ); ?>" />
<?php endif ?>
<?php if ( ! empty( $options['custom_apple_icon'] ) ) : ?>
<link rel="apple-touch-icon" href="<?php echo esc_attr( $options['custom_apple_icon'] ); ?>" />
<?php endif ?>
<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="menu">

	<header id="site-header">

		<nav class="off-canvas-nav"> 
			<span class="sidebar-item"><a class="sidebar-button" href="#sidebar" title="<?php echo esc_attr( __( 'Menu', 'renkon' ) ); ?>"><?php echo esc_attr( __( 'Menu', 'renkon' ) ); ?></a></span>
		</nav><!-- end .off-canvas-navigation -->

		<div class="site-title">
		<?php if ( ! empty( $options['custom_logo'] ) ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img src="<?php echo esc_url( $options['custom_logo'] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" /></a>
		<?php else : ?>
			<h1 class="title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo esc_attr( get_bloginfo( 'name' ) ); ?></a></h1>
		<?php endif; ?>
		</div>
	</header><!-- end #site-header -->

	<a class="mask-right" href="#menu"></a>

	<div class="column-wrap">

	<div class="container">

	<?php
	$header_image = get_header_image();
	if ( ! empty( $header_image ) && is_front_page() ) :
		?>
			<div id="header-image" style="background-image: url(<?php echo esc_url( $header_image ); ?>); " >
				<div class="header-outer">
					<div class="header-inner">
		<?php if ( ! empty( $options['header-slogan'] ) ) : ?>
							<p class="header-slogan"><?php echo esc_html( stripslashes( $options['header-slogan'] ) ); ?></p>
		<?php endif; ?>
		<?php if ( ! empty( $options['header-subtitle'] ) ) : ?>
							<p class="header-subtitle"><?php echo esc_html( stripslashes( $options['header-subtitle'] ) ); ?></p>
		<?php endif; ?>
		<?php if ( ! empty( $options['header-slogan'] ) ) : ?>
							<a href="#content-wrap" class="header-btn"><?php echo esc_html( __( 'Show Content', 'renkon' ) ); ?></a>
		<?php endif; ?>
					</div><!-- end .header-inner -->
				</div><!-- end .header-outer -->
			</div><!-- end .header-image -->
	<?php endif; ?>

		<div id="content-wrap">
