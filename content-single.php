<?php
/**
 * Content for pages and posts
 *
 * @package station.gallery
 */

$options = get_option( 'renkon_theme_options' );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<div class="entry-date"><?php echo get_the_date(); ?></div>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!--end .entry-header -->

	<div class="entry-content">
	<?php if ( is_search() ) : // Show excerpts on search results. ?>
		<?php the_excerpt(); ?>
	<?php else : ?>
		<?php the_content( __( 'Read more', 'renkon' ) ); ?>
		<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-link">' . __( 'Pages:', 'renkon' ),
				'after'  => '</div>',
			)
		);
		?>
	<?php endif; ?>
	</div><!-- end .entry-content -->

</article><!-- end post -<?php the_ID(); ?> -->
